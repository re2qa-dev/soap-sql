<?php
class SoapClientClass {
	
	function registerUserClientsoapcall ($username, $password, $first_name, $last_name) {
		
		//// soap web services URL /////
		$URL = 'http://localhost/Bitbucket/soap_service.php';
		$client = new SoapClient(null, array(
			'location' => $URL,
			'uri'      => "http://localhost/Bitbucket/",
			'trace'    => 1,
			));
		$params = array('Username'=>$username,'Password' => $password, 'Firstname' => $first_name, 'Lastname' => $last_name );
		// soap call wih param ////
		$return = $client->__soapCall("RegisterUser",$params, array('soapaction' => 'http://localhost/Bitbucket/soap_service.php'));
		echo '<pre>';
		echo PHP_EOL;
		echo "#### Request (WSDL Client): ####" . PHP_EOL;
		echo $this->pretty_xml($client->__getLastRequest());
		echo PHP_EOL;
		echo '<hr/>';
		echo "#### Response (WSDL Client): ####" . PHP_EOL;
		echo $this->pretty_xml($client->__getLastResponse());
	}
	
	function validateUserClientsoapcall($username, $password) {
		//// soap web services URL /////
		$URL = 'http://localhost/Bitbucket/soap_service.php';
		$client = new SoapClient(null, array(
			'location' => $URL,
			'uri'      => "http://localhost/Bitbucket/",
			'trace'    => 1,
			));
		/// parameters for validate function ////	
		$params = array('Username'=>$username,'Password' => $password);
		// soap call wih param ////
		$return = $client->__soapCall("ValidateUser",$params, array('soapaction' => 'http://localhost/Bitbucket/soap_service.php'));
		echo '<pre>';
		echo PHP_EOL;
		echo "#### Request (WSDL Client): ####" . PHP_EOL;
		echo $this->pretty_xml($client->__getLastRequest());
		echo PHP_EOL;
		echo '<hr/>';
		echo "#### Response (WSDL Client): ####" . PHP_EOL;
		echo $this-> pretty_xml($client->__getLastResponse());
	}
	
	function pretty_xml($uglyXml)
	{
		$dom = new DOMDocument();
		$dom-> preserveWhiteSpace = FALSE;
		$dom->loadXML($uglyXml);
		$dom->formatOutput = TRUE;
		return $dom->saveXml();
	}
}




?>