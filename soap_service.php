<?php
/*
 * Bitbucket.org Soap Server
*/

ini_set("soap.wsdl_cache_enabled", "0");
//// for sql Connection /////

include_once "/ez-mysql/shared/ez_sql_core.php";
include_once "/ez-mysql/mysql/ez_sql_mysql.php";


class SoapAPI {
	protected $db;
	function __construct() {
		
		//// db conection ///
		/// change my sql connection cred here
		$username = 'root';
		$password = '';
		$database = 'bitbucket';
		$hostname = 'localhost';
		
		$this->_db = new ezSQL_mysql($username,$password,$database,$hostname, 'utf-8'); 
		$current_time = $this->_db->get_var("SELECT " . $this->_db->sysdate());
	}
	
	
	/*  User Registration */
	function RegisterUser($username, $paswword, $first_name, $last_name) {
		
		if(trim($username) == "" || trim($paswword) == "" || trim($first_name) == ''){
			return 'Please Provide Proper information to register';
		} else {
			/// insert query ////
			$query_insert = "Insert into bitbucket_user set username = '".htmlspecialchars(addslashes($username))."', password = '".md5($paswword)."', first_name = '".htmlspecialchars(addslashes($first_name))."', last_name = '".htmlspecialchars(addslashes($last_name))."'";
			
			//// call ez sql function for insert query ////
			 $result = $this->_db->query($query_insert);
			 if($result == 1) {
				 return 'User Registered Successfully';
			 } else {
				 return 'Some Error occured on regisration';
			 }
		}
		
	}
	/*  Validate User */
	function ValidateUser($username, $paswword) {
		
		if(trim($username) == "" || trim($paswword) == ""){
			return 'User Not Validated';
		} else {
			$query_select= "Select username, password From bitbucket_user where username = '".htmlspecialchars(addslashes($username))."' And password = '".md5($paswword)."' Limit 1";
			
			//// call ez sql function for select query ////
			 $result = $this->_db->query($query_select);
			 if($result > 0) {
				 return 'User Validated Sucessfully'; 
			 } else {
				 return 'User Not Validated';
			 }
		}
		
	}
	
}

//when in non-wsdl mode the uri option must be specified
$options=array('uri'=>'http://localhost/Bitbucket'); //// addd your wweb service link
//create a new SOAP server
$server = new SoapServer(NULL,$options);
//attach the API class to the SOAP Server
$server->setClass('SoapAPI');
//start the SOAP requests handler
$server->handle();

?>